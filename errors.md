# Simple Note System, version 2
## Error Code Reference

### General Codes
| Name         | Code | Meaning                               |
|--------------|------|---------------------------------------|
| ERR_NO_STORE |  5   | the SNS store needs to be initialized |
|              |      |                                       |
| ERR_NO_OPTS  |  10  | No mode argument was specified        |
|              |      |   																		|
| ERR_NO_ARGS  |  11  | A mode requiring an argument was      |
|              |      | specified without an argument				  |
|              |      |                                       |

### Encryption-related codes
|Name        | Code | Meaning                                 |
|------------|------|-----------------------------------------|
| ERR_NO_GPG | 100  | Encryption is enabled, but GPG is not   |
|						 |      | installed       												|
|						 |			|			 																	  |
| ERR_NO_KEY | 110  | Encryption is enabled, but no recipient |
|            |      | was specified                           |

### Creation-related codes
|Name             | Code | Meaning                            |
|-----------------|------|------------------------------------|
| ERR_NOTE_EXiSTS | 200  | The specified note already exists  |
| ERR_NOTE_NO_READ| 205  | The specified note cannot be read  |
