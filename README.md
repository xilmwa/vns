Vim Note System
==================

## About
Vim Note System is a shell script partially inspired by [pass].
vns stores notes as asymmetrically encrypted plaintext using the
vim-gnupg plugin.

VNS, formerly SNS, was originally conceived one morning during an update 
to a popular note-taking app. The thought occurred that a note system need 
not reinvent the wheel with its own GUI editor and proprietary file format,
but instead could use the tools already provided by the operating system.

    vim note system
    ==================

    usage: vns [-cedlp] <notebook/section/name>
           vns [-hi]
           vns git ...

      -c | --create : Create note
      -d | --delete : Delete note
      -e | --edit   : Open note for editing
      -h | --help   : Display this message
      -i | --init   : Initialize note store"
      -l | --list   : List all notes in <notebook>
      -p | --print  : Print note to console

## Installing
  ### Dependencies
    * vim
    * gpg2
    * tree
    * git

  ** Note: ** Install [vim-gnupg] before use. vns *does* check this, but only *after*
              a note has been created.

  To install, place `vns` in your path, and run `vns -i`.

  vim note system will establish its note store under ~/.config/vns. 
  The location of the store may be changed on line 6.

  To uninstall, remove the files you copied. Notes will still exist in ~/.config/vns

## Tips and Tricks
* To list all notes in all notebooks, simply run `vns`.
* Notes beginning with . will not appear in the listing

## Credits
The code here is my own, however much of VNS's design and behavior was
influenced by [pass].

## License
Vim Note System is licensed under the terms of the GNU General Public License
Version 2, as detailed in `LICENSE`.

## Bugs and Feature Requests
If something seems off, or just doesn't work, please open an issue and I'll look
into it.

Feature requests should be submitted to jon.lewis@xenami.net with [SNS Feature Request]
in the subject line.

[pass]: http://passwordstore.org
[vim-gnupg]:http://www.vim.org/scripts/script.php?script_id=3645
